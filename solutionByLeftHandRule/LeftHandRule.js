let currentNodeIndexLHR=0, nextNodeIndexLHR=0;
let leftLHR=2, forwardLHR=1, rightLHR=0;//Initially left is top and right is bottom & forward in the direction orginal right
let pathArrayLHR=[];
function solveByLeftHandRule(){
    if(currentNodeIndexLHR==cols*rows-1){
        showPath(pathArrayLHR);
        noLoop();
    }
    current=grid[currentNodeIndexLHR];
    current.highlight();

    //showAdotAtFront();
    //if there is no left wall
    if(grid[currentNodeIndexLHR] && !grid[currentNodeIndexLHR].walls[leftLHR]){
        rotate90degCW();//rotate 90deg counterclockwise and move forward
        moveforward();
        pathArrayLHR.push(current);

    } else if(grid[currentNodeIndexLHR] && !grid[currentNodeIndexLHR].walls[forwardLHR]){
        moveforward();
        pathArrayLHR.push(current);
    }
    else {
        rotate90degCCW();
    }
    currentNodeIndexLHR=nextNodeIndexLHR;
}

function showAdotAtFront(){
    stroke(255);
    if(forwardLHR==0) circle(current.i*w, current.j*w+w/2, w/5);
    if(forwardLHR==1) circle(current.i*w+w, current.j*w+w/2, w/5);
    if(forwardLHR==2) circle(current.i*w+w, current.j*w+w/2, w/5);
    if(forwardLHR==3) circle(current.i*w/2, current.j*w, w/5);
}
function rotate90degCW(){
    if((leftLHR+1)<0)       leftLHR=(((leftLHR+1)%4)+4)%4;
    else                leftLHR=(leftLHR+1)%4;
    
    if((forwardLHR+1)<0)    forwardLHR=(((forwardLHR+1)%4)+4)%4;
    else                forwardLHR=(forwardLHR+1)%4;
    
    if((rightLHR+1)<0)      rightLHR=(((rightLHR+1)%4)+4)%4;
    else                rightLHR=(rightLHR+1)%4;
}
function rotate90degCCW(){
    if((leftLHR-1)<0)       leftLHR=(((leftLHR-1)%4)+4)%4;
    else                leftLHR=(leftLHR-1)%4;
    
    if((forwardLHR-1)<0)    forwardLHR=(((forwardLHR-1)%4)+4)%4;
    else                forwardLHR=(forwardLHR-1)%4;
    
    if((rightLHR-1)<0)      rightLHR=(((rightLHR-1)%4)+4)%4;
    else                rightLHR=(rightLHR-1)%4;
}

function moveforward() {

    if (forwardLHR == 0) {
        nextNodeIndexLHR = moveup(currentNodeIndexLHR);
    } else if (forwardLHR == 1) {
        nextNodeIndexLHR = moveright(currentNodeIndexLHR);
    } else if (forwardLHR == 2) {
        nextNodeIndexLHR = movebottom(currentNodeIndexLHR);
    } else if (forwardLHR == 3) {
        nextNodeIndexLHR = moveleft(currentNodeIndexLHR);
    }

}

function moveup(index){

    index=cols * (floor(index / cols) - 1) + index % cols;

    //if(grid[index]) grid[index].highlight();

    return index;
}
function moveright(index){
    index++
    //if(grid[index]) grid[index].highlight();
    return index;
}
function movebottom(index){

    index=cols * (floor(index / cols) + 1) + index % cols

    //if(grid[index]) grid[index].highlight();
    return index;
}
function moveleft(index){
    index--;
    //if(grid[index]) grid[index].highlight();
    return index;
}

function isEqualNode(node1, node2) {
    return node1.i == node2.i && node1.j == node2.j;
}