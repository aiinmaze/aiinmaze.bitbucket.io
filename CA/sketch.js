// Daniel Shiffman
// http://codingtra.in
// http://patreon.com/codingtrain

// Game of Life
// Video: https://youtu.be/FWSR_7kZuYg

function make2DArray(cols, rows) {
    let arr = new Array(cols);
    for (let i = 0; i < arr.length; i++) {
      arr[i] = new Array(rows);
    }
    return arr;
  }
  
  let grid;
  let cols;
  let rows;
  let resolution = 10;
  
  function setup() {
    createCanvas(100, 100);
    cols = width / resolution;
    rows = height / resolution;
  
    grid = make2DArray(cols, rows);
    for (let i = 0; i < cols; i++) {
      for (let j = 0; j < rows; j++) {
        grid[i][j] = floor(random(2));
      }
    }
  }
  
  function draw() {
    background(0);
  
    for (let i = 0; i < cols; i++) {
      for (let j = 0; j < rows; j++) {
        let x = i * resolution;
        let y = j * resolution;
        if (i==0 || j==0 || i== cols-1 || j==rows-1 || grid[i][j] == 1) {
          fill(255);
          if(i==1 && j==0) fill(255,0,0);
          if(i==cols-2 && j==rows-1) fill(255,0,0);
          stroke(0);
          rect(x, y, resolution - 1, resolution - 1);
        }
      }
    }
  
    let next = make2DArray(cols, rows);
  
    // Compute next based on grid
    for (let i = 0; i < cols; i++) {
      for (let j = 0; j < rows; j++) {
        let state = grid[i][j];
        // Count live neighbors!
        let sum = 0;
        let neighbors = countNeighbors(grid, i, j);
  
        // if (state == 0 && neighbors == 3) {
        //   next[i][j] = 1;
        // } else if (state == 1 && (neighbors < 2 || neighbors > 3)) {
        //   next[i][j] = 0;
        // } else {
        //   next[i][j] = state;
        // }
        
        if(state == 0 && neighbors==3){
          next[i][j]=1;
        }
        else if(state == 1 &&  (neighbors=>1 && neighbors<=5)){
          next[i][j]=state;
        }
        else{
          next[i][j]=0;
        }

      }
    }
  
    grid = next;
  
    console.log("generation:"+(generation++));
  }
  let generation=1;
  
  
  function countNeighbors(grid, x, y) {
    let sum = 0;
    if(grid[x-1] && grid[x-1][y-1]) sum+=grid[x-1][y-1];
    if(grid[x] && grid[x][y-1]) sum+=grid[x][y-1];
    if(grid[x+1] && grid[x+1][y-1]) sum+=grid[x+1][y-1];
    
    if(grid[x+1] && grid[x+1][y+1]) sum+=grid[x+1][y+1];
    if(grid[x] && grid[x][y+1]) sum+=grid[x][y+1];
    
    if(grid[x-1] && grid[x-1][y+1]) sum+=grid[x-1][y+1];
    if(grid[x-1] && grid[x-1][y]) sum+=grid[x-1][y];
    return sum;
  }