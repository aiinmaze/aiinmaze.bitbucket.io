let edgeList = new Array();
let set = disjointSet();
let maxIndex;
function generateGridByKRUSKAL() {

    //if there is just single set exist just return
    if (maxIndex < 1) {
        return;
    }

    if (edgeList && edgeList.length == 0) {
        //console.log(grid);
        for (var i = 0; i < grid.length * 4; i++) {
            //edge is available or not
            if (!(grid[Math.floor(i / 4)].i == 0 && i % 4 == 3) && !(grid[Math.floor(i / 4)].i == cols - 1 && i % 4 == 1) && !(grid[Math.floor(i / 4)].j == 0 && i % 4 == 0) && !(grid[Math.floor(i / 4)].j == rows - 1 && i % 4 == 2)) {
                edgeList.push(i);
            }
        }

        maxIndex = edgeList.length;

        for (var i = 0; i < grid.length; i++) {
            set.add(grid[i]);
        }
        //debugger;
        //console.log("edgelist",edgeList);
    }
    //for each wall in some random order
    let randomIndex = Math.floor(random(maxIndex));
    //console.log(randomIndex);                       
    let randomEdge = edgeList[randomIndex];
    let randomNode = Math.floor(randomEdge / 4);
    //console.log("randomindex:"+randomIndex,randomEdge,"length:"+maxIndex); 
    //debugger;

    //If the cells divided by this wall belong to distinct sets:
    let node1 = grid[Math.floor(randomEdge / 4)];
    current = node1;
    current.highlight();
    let node2Index;
    let mode = (randomEdge - Math.floor(randomEdge / 4) * 4);//top left bottom right

    if (mode == 0) {

        if (grid[cols * (floor(randomNode / cols) - 1) + randomNode % cols]) {
            node2Index = cols * (floor(randomNode / cols) - 1) + randomNode % cols;
        }
        //("check top edge");

    } else if (mode == 1) {
        if (grid[randomNode + 1]) {
            node2Index = randomNode + 1;
        }
        //check for right node;

    } else if (mode == 2) {

        if (grid[cols * (floor(randomNode / cols) + 1) + randomNode % cols]) {
            node2Index = cols * (floor(randomNode / cols) + 1) + randomNode % cols;
        }
        //check for bottom edge

    } else if (mode == 3) {

        if (grid[Math.floor(randomEdge / 4) - 1]) {
            node2Index = Math.floor(randomEdge / 4) - 1;
        }
        //check for left
    }
    let node2 = grid[node2Index];
    if (grid[node2Index] && !set.connected(node1, node2)) {
        //Remove the current wall.
        removeWalls(node1, node2);
        set.union(node1, node2);
    }
    removeEdge(randomIndex);
    maxIndex = edgeList.length;
}

function removeEdge(randomIndex) {
    edgeList.splice(randomIndex, 1);
}
