var cols, rows;
var w = 30;
var grid = [];

var current;

var stack = [];
let path = [];
let foundsolution = false;

function setup() {

  var _w = $("#col1").width();

  let cnv = createCanvas(_w, _w);
  cnv.parent('cnvContainer');

  cols = floor(width / w);
  rows = floor(height / w);
  frameRate(60);//Max-framerate is 60

  for (var j = 0; j < rows; j++) {
    for (var i = 0; i < cols; i++) {
      var cell = new Cell(i, j);
      grid.push(cell);
    }
  }
  current = grid[0];
}

function showPath(path) {
  noFill();
  strokeWeight(w / 2);
  strokeJoin(ROUND);// BEVEL   MITER
  beginShape();
  vertex(w / 2, 0);
  stroke(255, 0, 0, 100);
  path.forEach(element => {
    vertex(element.i * w + w / 2, element.j * w + w / 2);
  });
  vertex(rows * w - w / 2, cols * w - w / 2)
  vertex(rows * w, cols * w - w / 2)
  endShape();
}
let objgenerate;
let objgenerate1;
let visitedAll = true;
function draw() {
  
  background(10);
  visitedAll = true;
  for (var i = 0; i < grid.length; i++) {
    grid[i].show();
    //debugger;
    visitedAll = visitedAll && grid[i].visited;
  }
  if (path && foundsolution) {
    showPath(path);
  }

  if (generate) {
    //debugger;
    generateGrid();
    //if(!objgenerate) objgenerate = new Sidewinder()
    //objgenerate1.generate();
  }


  if (solveflag) {
     generate = false;
    solveGrid();

  }

  strokeWeight(1);

}

let generate = false;
let solveflag = false;

let allCapableAlgorithm = {
  "DFS": "DFS",
  "KRUSKAL": "KRUSKAL",
  "PRIMSALGORITHM": "PRIM'S ALGORITHM"
}

let allCapableSolvingAlgorithm = {
  "DFS": "DFS",
  "LEFTHANDRULE": "LEFT HAND RULE",
  "GA": "GENETIC ALGORITHM"
}

let generateByAlgorithm = allCapableAlgorithm.DFS;
let solveByAlgorithm = allCapableSolvingAlgorithm.DFS;


function possibleTovisit(a, b) {

  if ((a.i == cols - 1 && b.i == 0) || (a.i == 0 && b.i == cols - 1)) {
    return false;
  }

  var x = a.i - b.i;
  if (x === 1) {
    return !a.walls[3] && !b.walls[1];
  } else if (x === -1) {
    return !a.walls[1] && !b.walls[3];
  }
  var y = a.j - b.j;
  if (y === 1) {
    return !a.walls[0] && !b.walls[2]
  } else if (y === -1) {
    return !a.walls[2] && !b.walls[0];
  }
}

function index(i, j) {
  if (i < 0 || j < 0 || i > cols - 1 || j > rows - 1) {
    return -1;
  }
  return i + j * cols;
}

function _resizeCanvas() {
  //setTimeout(() => {
    resetCanvas();
  //}, 2000);
}

function generateGrid() {
  
  switch (generateByAlgorithm) {
    case "DFS":
      generateGridByDFS();
      break;
    case "KRUSKAL":
      generateGridByKRUSKAL();
      break;
      case "PRIMSALGORITHM":
      objgenerate.generate();
      break;
    default:
      break;
  }
}
function genAlgorithmSwitch() {
  //just to refresh the canvas
  //resetCanvas();
  generate = true;
  generateByAlgorithm = document.getElementById('gridgenerationAlgorithm').value;

  if(generateByAlgorithm == "PRIMSALGORITHM"){
    objgenerate = new GridByPRIM();
  }

}

function solveGrid() {
  switch (solveByAlgorithm) {
    case allCapableSolvingAlgorithm.DFS:
      dfs();
      break;
    case allCapableSolvingAlgorithm.LEFTHANDRULE:
      solveByLeftHandRule();
      break;
    case allCapableSolvingAlgorithm.GA:
      solvebyGA();
      break;
    default:
      break;
  }
}

function solveAlgorithmSwitch(algo) {
  solveflag = true;
  switch (algo) {
    case "DFS":
      solveByAlgorithm = allCapableSolvingAlgorithm.DFS;
      break;
    case "LEFTHANDRULE":
      solveByAlgorithm = allCapableSolvingAlgorithm.LEFTHANDRULE;
      break;
    
    default:
      break;
  }

}

function resetCanvas() {
  if (parseInt(document.getElementById('heightOfCanvas').value) < 100 || parseInt(document.getElementById('widthOfCanvas').value) < 100) {
    alert("value of canvas width and height can not be less then 100");
    return;
  }
  w = +document.getElementById('widthOfEachBlock').value;
  if (w < 20) {
    alert("block width cannot be less then 20");
    $('#widthOfEachBlock').val(30);
    w = 30;
    return;
  }
  resizeCanvas(+document.getElementById('heightOfCanvas').value, +document.getElementById('widthOfCanvas').value);
  cols = floor(width / w);
  rows = floor(height / w);
  grid = [];
  for (var j = 0; j < rows; j++) {
    for (var i = 0; i < cols; i++) {
      var cell = new Cell(i, j);
      grid.push(cell);
    }
  }
  current = grid[0];
}