function Cell(i, j) {
  this.i = i;
  this.j = j;
  this.walls = [true, true, true, true];
  this.visited = false;

  this.checkNeighbors = function(gridArray) {
    var neighbors = [];

    var top    = gridArray[index(i, j -1)];
    var right  = gridArray[index(i+1, j)];
    var bottom = gridArray[index(i, j+1)];
    var left   = gridArray[index(i-1, j)];

    if (top && !top.visited) {
      neighbors.push(top);
    }
    if (right && !right.visited) {
      neighbors.push(right);
    }
    if (bottom && !bottom.visited) {
      neighbors.push(bottom);
    }
    if (left && !left.visited) {
      neighbors.push(left);
    }

    if (neighbors.length > 0) {
      var r = floor(random(0, neighbors.length));
      return neighbors[r];
    } else {
      return undefined;
    }


  }
  this.highlight = function() {
    var x = this.i*w;
    var y = this.j*w;
    noStroke();
    fill(255, 255, 0, 95);
    rect(x, y, w, w);

  }

  this.show = function() {
    var x = this.i*w;
    var y = this.j*w;
    stroke(255);
    if (this.i!=0 &&this.walls[0]) {
      line(x    , y    , x + w, y);
    }
    //debugger;
    if (this.walls[1]) {
      line(x + w, y    , x + w, y + w);
    }
    if (this.walls[2]) {
      line(x + w, y + w, x    , y + w);
    }
    if (this.walls[3]) {
      line(x    , y + w, x    , y);
    }
    

    line(w*rows-1,0,w*rows-1,w*cols-1-w);
    line(0,w*cols-1,w*rows-1,w*cols-1);

    if (this.visited) {
      noStroke();
      fill(255, 255, 255, 100);
      rect(x, y, w, w);
    }

  }
}
