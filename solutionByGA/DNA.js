
function newChar() {
    var c = Math.floor(random(0, 4));
    return c;
}
let populationIndex=0;
function DNA(num) {
    this.genes = [];
    this.fitness = 0;
    this.tailIndex=0;
    this.populationIndex=populationIndex++;
    populationIndex=this.populationIndex;

    for (var i = 0; i < num; i++) {
        this.genes[i] = newChar();
    }

    this.getPhrase = function () {
        return this.genes.join("");
    }

    this.fitnessfunction = function () {
        //debugger;
        var score = 0;
        var tailIndextemp=1;
        let currentNodeIndex = 0;
        let nextNodeIndex;
        let visitedAlready=new Array(this.genes.length);
        visitedAlready.fill(false);
        visitedAlready[0]=true;
        for (let i = 0; i < this.genes.length; i++) {
            if (this.genes[i] == "0") {
                nextNodeIndex = moveup(currentNodeIndex);
            } else if (this.genes[i] == "1") {
                nextNodeIndex = moveright(currentNodeIndex);
            } else if (this.genes[i] == "2") {
                nextNodeIndex = movebottom(currentNodeIndex);
            } else if (this.genes[i] == "3") {
                nextNodeIndex = moveleft(currentNodeIndex);
            }

            if (!visitedAlready[nextNodeIndex] && grid[nextNodeIndex] && possibleTovisit(grid[currentNodeIndex], grid[nextNodeIndex]) ) {
                tailIndextemp++;
                //=10000-index(grid[nextNodeIndex].i,grid[nextNodeIndex].j)-cols*rows-1;
                
                if(!isThreeSideWall(grid[nextNodeIndex]) || grid[nextNodeIndex].i!=0 || grid[nextNodeIndex].i!=cols-1 || grid[nextNodeIndex].j!=0 || grid[nextNodeIndex].j!=rows-1)
                    score=euclidean(grid[grid.length-1].i,grid[grid.length-1].j,grid[currentNodeIndex].i,grid[currentNodeIndex].j);
                
                score=1/Math.floor(score);
                currentNodeIndex=nextNodeIndex;
                visitedAlready[nextNodeIndex]=true;
            }else {
                //score+=euclidean(grid[grid.length-1].i,grid[grid.length-1].j,grid[currentNodeIndex].i,grid[currentNodeIndex].j);
                break;
            };
        }
        //this.fitness = score / cols*rows;
        this.tailIndex = tailIndextemp;
        this.fitness = score;
    }

    this.crossover = function (partner) {

        //debugger;

        var child = new DNA(this.genes.length);

        var midpoint = floor(random(this.genes.length));

        for (let i = 0; i < this.genes.tailIndex; i++) {

            if (i > midpoint) child.genes[i] = this.genes[i];
            else child.genes[i] = partner.genes[i];

        }
        return child;
    }

    this.mutate = function (mutateRate) {
        //debugger;
        for (let i = 0; i < this.genes.length; i++) {
            if (random(1) < mutateRate) {
                this.genes[i]=newChar();
            }

        }
    }

}

function distancefromEndNode(NodeIndex){
    return Math.abs(cols*rows-NodeIndex-1);
}

function manhattan(x1,y1,x2,y2){
    return Math.abs(x1-x2) + Math.abs(y1-y2);
}

function euclidean(x1,y1,x2,y2){
    return Math.sqrt((x2-x1)*(x2-x1) + (y2-y1)*(y2-y1));
}

function mapFromTo(x,a,b,c,d){
    return (x-a)/(b-a)*(d-c)+c;
}

function isThreeSideWall(node){
    //count number of true if one true then return true else false
    let count=0;
    for(let i=0;i<4;i++){
        if(node.walls[i]) count++;
    }
    return count==3;
}