
function Population(m, num) {

    this.mutationRate = m;
    this.population = new Array(num);
    for (var i = 0; i < this.population.length; i++) {
        this.population[i] = new DNA(rows*cols);
    }

    this.matingPool = new Array();
    this.finished = false;
    this.generations = 0;



    // Fill our fitness array with a value for every member of the population
    this.calcFitness = function () {
        for (var i = 0; i < this.population.length; i++) {
            this.population[i].fitnessfunction();
        }
    }

    this.naturalSelection = function () {
        
        this.calcFitness();
        this.matingPool = [];

        var maxFitness = 0;
        var maxindex=0;

        for (let i = 0; i < this.population.length; i++) {
            if (this.population[i].fitness > maxFitness) {
                maxFitness = this.population[i].fitness;
                maxindex=i;
            }
        }
        console.log(this.population[maxindex].fitness,this.population[maxindex].genes);
        let pathArraytemp=[];
        pathArraytemp.push(grid[0]);
        let currentNodeIndex=0;
        for(let i=0;i<this.population[maxindex].tailIndex-1;i++){
            let nextNodeIndex;
            if (this.population[maxindex].genes[i] == "0") {
                nextNodeIndex = moveup(currentNodeIndex);
            } else if (this.population[maxindex].genes[i] == "1") {
                nextNodeIndex = moveright(currentNodeIndex);
            } else if (this.population[maxindex].genes[i] == "2") {
                nextNodeIndex = movebottom(currentNodeIndex);
            } else if (this.population[maxindex].genes[i] == "3") {
                nextNodeIndex = moveleft(currentNodeIndex);
            }
            currentNodeIndex=nextNodeIndex;
            pathArraytemp.push(grid[currentNodeIndex]);
        }
        //debugger;
        showPath(pathArraytemp);
        //debugger;

        for (let i = 0; i < this.population.length; i++) {
            //debugger;
            
            var fitness = map(this.population[i].fitness, 0, maxFitness, 0, 1);

            var n = floor(fitness * 100);

            for (var j = 0; j < n; j++) {

                this.matingPool.push(this.population[i]);

            }

        }

    }


    this.generate = function () {

        for (var i = 0; i < this.population.length; i++) {

            var a = floor(random(this.matingPool.length));
            var b = floor(random(this.matingPool.length));
            var partnerA = this.matingPool[a];
            var partnerB = this.matingPool[b];

            var child = partnerA.crossover(partnerB);
            child.mutate(this.mutationRate);
            this.population[i] = child;
        }
        this.generatations++;

    }


    this.finished = function () {
        return finished;
    }
}

