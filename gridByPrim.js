//Start with a grid full of walls.

class GridByPRIM {
    constructor() {
        this.maxIndex = 0;////
        
        current=grid[floor(random(cols*rows))];
        this.walls=[];
        this.addWalls(current);
        current.visited=true;
    }

    generate(){
        if(this.walls.length==0){
            return;
        }
        //console.log(this.walls);
        let randomWallIndex=Math.floor(random(this.walls.length));
        let node1 = grid[Math.floor(this.walls[randomWallIndex] / 4)];
        let node2=grid[getNode2Index(this.walls[randomWallIndex])];
        debugger;
        if((node1.visited && !node2.visited) || (!node1.visited && node2.visited)){
            removeWalls(node1, node2);
            if(!node1.visited){
                this.addWalls(node1);
                node1.visited=true;
            }
            else if(!node2.visited){
                this.addWalls(node2);
                node2.visited=true;
            }
        }
        this.removeWall(randomWallIndex);
        
    }

    addWalls(node){
        //debugger;
        if(node.j!=0 && node.walls[0]){
            this.walls.push(index(node.i,node.j)*4);
        }
        if(node.i!=cols-1 && node.walls[1]){
            this.walls.push(index(node.i,node.j)*4+1);
        }
        if(node.j!=rows-1 && node.walls[2]){
            this.walls.push(index(node.i,node.j)*4+2);
        }
        if(node.i!=0 && node.walls[3]){
            this.walls.push(index(node.i,node.j)*4+3);
        }
    }
    removeWall(index) {
        this.walls.splice(index, 1);
    }
}

function getNode2Index(randomEdge){
    let node2Index;
    let randomNode = Math.floor(randomEdge / 4);
    let mode = (randomEdge - randomNode * 4);//top left bottom right
    
    if (mode == 0) {

        if (grid[cols * (floor(randomNode / cols) - 1) + randomNode % cols]) {
            node2Index = cols * (floor(randomNode / cols) - 1) + randomNode % cols;
        }
        //("check top edge");

    } else if (mode == 1) {
        if (grid[randomNode + 1]) {
            node2Index = randomNode + 1;
        }
        //check for right node;

    } else if (mode == 2) {

        if (grid[cols * (floor(randomNode / cols) + 1) + randomNode % cols]) {
            node2Index = cols * (floor(randomNode / cols) + 1) + randomNode % cols;
        }
        //check for bottom edge

    } else if (mode == 3) {

        if (grid[randomNode - 1]) {
            node2Index = randomNode - 1;
        }
        //check for left
    }
    return node2Index;
}