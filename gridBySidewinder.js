class Sidewinder{
    constructor(){
        this.RunSet = new Set();
        current=grid[cols];
        this.RunSet.add(current);
    }

    generate(){
        if(current.i+1 >= cols){
            current=grid[index(current.i,current.j+1)];
        }
        let decideCarveEast = floor(random(2))==1 && current.i+1 < cols;
        let nextNode;
        if(decideCarveEast){
            nextNode = grid[index(current.i+1,current.j)];
            removeWalls(current, nextNode);
            this.RunSet.add(nextNode);
            current=nextNode;
        }
        else{
            if(this.RunSet.size<1 || current.i+1 >= cols) return;
            let tempcurrent=current;
            let randomIndex = random(this.RunSet.size);
            let i=0;
            var setIter = this.RunSet.values();
            let northNode;
            while(i<=randomIndex){
                current = setIter.next().value;
                i++;
            }
            northNode=grid[index(current.i,current.j-1)];
            removeWalls(current,northNode);
            current=grid[index(tempcurrent.i+1, tempcurrent.j)];
            this.RunSet.clear();
        }
    }
}