let gridCopy;
current = grid[0];
function checkNeighborsForDFS(gridArray, c) {
  var neighbors = [];
  i = c.i;
  j = c.j;
  var top = gridArray[index(i, j - 1)];
  var right = gridArray[index(i + 1, j)];
  var bottom = gridArray[index(i, j + 1)];
  var left = gridArray[index(i - 1, j)];

  if (top && !top.visited && possibleTovisit(c, top)) {
    neighbors.push(top);
  }
  if (right && !right.visited && possibleTovisit(c, right)) {
    neighbors.push(right);
  }
  if (bottom && !bottom.visited && possibleTovisit(c, bottom)) {
    neighbors.push(bottom);
  }
  if (left && !left.visited && possibleTovisit(c, left)) {
    neighbors.push(left);
  }

  if (neighbors.length > 0) {
    var r = floor(random(0, neighbors.length));
    return neighbors[r];
  } else {
    return undefined;
  }
}

function dfs() {
  let flag = false;
  strokeWeight(2);
  if (!gridCopy) {
    stack = [];
    gridCopy = grid;
    gridCopy.forEach(element => {
      element.visited = false;
    });
    current = gridCopy[0];
  }
  current.visited = true;
  current.highlight();

  if (current.i == rows - 1 && current.j == cols - 1) {
    path = stack;
    path.push(current);
    solvebydfs = false;
    foundsolution = true;
  }

  // STEP 1
  var next = checkNeighborsForDFS(gridCopy, current);
  if (next) {
    next.visited = true;

    // STEP 2
    stack.push(current);

    // STEP 3
    stroke(255);
    ellipse(current.i * w, current.j * w, 50, 50);


    // STEP 4
    current = next;
  } else if (stack.length > 0) {
    current = stack.pop();
  }
}